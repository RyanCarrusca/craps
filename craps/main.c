/*Game of craps*/


#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MIN_BET 5

int getWallet(void);
int makeBet(int wallet);
int playRound(void);
int doAgain(void);
void goodbye(int wallet);
int rollForPoint(int point);
int rollDice(void);
int rollDie(void);

int main(void) {
	int bet;
	int wallet = getWallet();

	do {
		bet = makeBet(wallet);

		if (playRound()) {
			wallet += bet;
		}
		else {
			wallet -= bet;
		}

		if (wallet < MIN_BET) {
			break;
		}

	} while (doAgain());

	goodbye(wallet);

	system("pause");
	return 0;
}


int getWallet(void) {
	int wallet;
	
	printf("How much money do you have to play with?\n");
	scanf("%d", &wallet);

	while (wallet < MIN_BET) {
		printf("Error! Wallet must have at least $%d. Enter another value: ", MIN_BET);
		scanf("%d", &wallet);
	}
	
	return wallet;
}


int makeBet(int wallet) {
	int bet;
	while (1) {
		printf("\nYou have $%d in your wallet.\nPlace your bet (minimum $%d): ", wallet, MIN_BET);
		scanf("%d", &bet);
		if (bet < MIN_BET) {
			printf("Your bet is below the minimum of $%d. Try again.\n", MIN_BET);
		}
		else if (bet > wallet) {
			printf("You've bet more than you have in your wallet. Try again.\n");
		}
		else {
			return bet;
		}
	}
}


int playRound(void) {
	int point = rollDice();
	printf("\nYou rolled a %d\n\n", point);

	if (point == 2 || point == 3 || point == 12) {
		printf("You lose :(\n\n");
		return 0;
	}
	else if (point == 7 || point == 11) {
		printf("You win :)\n\n");
		return 1;
	}
	else {
		return rollForPoint(point);
	}
}


int doAgain(void) {
	int again;
	printf("Enter 1 to play again, 0 to quit:\n");
	scanf("%d", &again);
	return again;
}


void goodbye(int wallet) {
	if (wallet < MIN_BET) {
		printf("You went broke. Goodbye!\n");
	}
	else {
		printf("You have $%d left in your wallet. Goodbye!\n", wallet);
	}
}


int rollForPoint(int point) {
	int roll;

	printf("Rolling for point: %d ...\n", point);

	while (1) {
		roll = rollDice();
		printf("You rolled a %d\n", roll);

		if (roll == point) {
			printf("You win :)\n\n");
			return 1;
		}
		else if (roll == 7) {
			printf("You lose :(\n\n");
			return 0;
		}
	}
}


int rollDice(void) {
	return rollDie() + rollDie();
}


int rollDie(void) {
	return rand() % 6 + 1;
}